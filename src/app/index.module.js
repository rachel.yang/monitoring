(function() {
  'use strict';

  angular
    .module('zeroWeb',
      [
        'ngAnimate',
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'toastr',
        'zeroWeb-monitoring']);

})();
