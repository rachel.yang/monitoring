(function() {
  'use strict';

  angular
    .module('zeroWeb')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController'
      })
      .state('login', {
        url : '/login',
        templateUrl : 'app/components/login/login.html',
        controller : 'LoginController'
      })
      .state('signup', {
        url : '/signup',
        templateUrl : 'app/components/signup/signup.html',
        controller : 'SignupController'
      })
    ;

    $urlRouterProvider.otherwise('/');
  }

})();
