/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('zeroWeb')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
