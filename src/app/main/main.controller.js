(function() {
  'use strict';

  angular
    .module('zeroWeb')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope) {
    console.log("MainController start");
  }
})();
