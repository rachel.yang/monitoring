/**
 * Created by yangyoomi on 2015. 12. 22..
 */
(function() {
  'use strict';

  angular
    .module('zeroWeb')
    .controller('SignupController', SignupController);

  /** @ngInject */
  function SignupController($scope, $state, $http) {
    console.log("SignupController start");

    $scope.role = [
      { name:'admin' },
      { name:'user' },
      { name:'guest' }
    ];

    $scope.signup = function () {

      var signupParams = {
        "name": $scope.id,
        "username": $scope.username,
        "password": $scope.password,
        "email": $scope.email,
        "role": $scope.role.userSelect
      };

      $http({
        method  : 'POST',
        url     : 'http://61.39.74.101:3000/register',
        data    : signupParams
        //headers : {'Content-Type': 'application/x-www-form-urlencoded'}
      })
        .then(
          function (resp) {
            console.log("Success");
          },
          function (reason) {
            console.error("error", reason.result);
            alert(reason.result.error.message);
          }
        );

      //console.log(signupParams);
      //$state.go("login");
    };
  }
})();
