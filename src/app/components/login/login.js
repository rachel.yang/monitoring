/**
 * Created by yangyoomi on 2015. 12. 15..
 */
(function() {
  'use strict';

  angular
    .module('zeroWeb')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController() {
    console.log("LoginController start");
  }
})();
