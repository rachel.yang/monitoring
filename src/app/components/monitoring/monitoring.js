/**
 * Created by yangyoomi on 2015. 12. 21..
 */
(function() {
  'use strict';

  angular
    .module('zeroWeb-monitoring', ['leaflet-directive'])
    .config(monitoringRouterConfig)
    .controller('MonitoringController', MonitoringController)
    .directive('resizable', resizable);

  function monitoringRouterConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('monitoring', {
        abstract : true,
        templateUrl : 'app/components/monitoring/monitoring.html',
        controller : 'MonitoringController'
      })
      .state('monitoring.map', {
        url : '/map',
        templateUrl : 'app/components/monitoring/map/map.html',
        controller : 'MapController'
      })
      .state('monitoring.energyConsumtion', {
        url : '/energyConsumtion',
        templateUrl : 'app/components/monitoring/energyConsumtion/energyConsumtion.html',
        controller : 'EnergyConsumtionController'
       })
    ;
    $urlRouterProvider.otherwise('/');
  }

  /** @ngInject */
  function MonitoringController($scope) {
    console.log("MonitoringController start");

    $scope.sidebar = true;
    $scope.clickedSidebar = function(){
      $scope.sidebar = !$scope.sidebar;
    };
  }

  /** @ngInject */
  function resizable($window) {
    return function ($scope) {
      $scope.initializeWindowSize = function () {
        $scope.windowHeight = $window.innerHeight;
        $scope.windowWidth = $window.innerWidth;
      };
      $scope.initializeWindowSize();
      return angular.element($window).bind("resize", function () {
        $scope.initializeWindowSize();
        console.log('now window height: ', $scope.windowHeight);
        return $scope.$apply();
      });
    };
  }

})();
