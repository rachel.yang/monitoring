/**
 * Created by yangyoomi on 2015. 12. 17..
 */
(function() {
  'use strict';

  angular
    .module('zeroWeb-monitoring')
    .controller('MapController', MapController);

  /** @ngInject */
  function MapController($scope, $http) {
    console.log("MapController Start");

    angular.extend($scope, {
      korea: {
        lat: 37.632,
        lng: 127.078,
        zoom: 10
      },
      /*layers: {
        baselayers: {
          googleTerrain: {
            name: 'Google Terrain',
            layerType: 'TERRAIN',
            type: 'google'
          },
          googleHybrid: {
            name: 'Google Hybrid',
            layerType: 'HYBRID',
            type: 'google'
          },
          googleRoadmap: {
            name: 'Google Streets',
            layerType: 'ROADMAP',
            type: 'google'
          }
        }
      },*/
      defaults: {
        scrollWheelZoom: false
      }
    });

  }

})();
