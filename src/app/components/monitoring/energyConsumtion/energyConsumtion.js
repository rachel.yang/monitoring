/**
 * Created by yangyoomi on 2015. 12. 21..
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb-monitoring')
    .controller('EnergyConsumtionController', EnergyConsumtionController);

  /** @ngInject */
  function EnergyConsumtionController($scope) {
    console.log("energyConsumtionController start");

    var chart1 = c3.generate({
      bindto: '#chart1',
      size: {
        width: 500
      },
      data: {
        columns: [
          ['data1', 30, 200, 100, 400, 150, 250],
          ['data2', 50, 20, 10, 40, 15, 25]
        ],
        axes: {
          data2: 'y2'
        },
        types: {
          data2: 'bar' // ADD
        }
      },
      axis: {
        y: {
          label: {
            text: 'Y Label',
            position: 'outer-middle'
          }
        },
        y2: {
          show: true,
          label: {
            text: 'Y2 Label',
            position: 'outer-middle'
          }
        }
      }
    });

    var chart2 = c3.generate({
      bindto: '#chart2',
      size: {
        width: 300
      },
      data: {
        columns: [
          ['data1', 30, 200, 100, 400, 150, 250],
          ['data2', 50, 20, 10, 40, 15, 25]
        ],
        types: {
          data1: 'bar'
        }
      },
      axis: {
        rotated: true
      }
    });

    var chart3 = c3.generate({
      bindto: '#chart3',
      size: {
        width: 150,
        height: 180
      },
      padding: {
        top: 20,
        right: 20
      },
      color: {
        pattern: ['#9467BD', '#AC9CBB']
      },
      data: {
        columns: [
          ['data1', 30],
          ['data2', 120]
        ],
        type: 'donut',
        onclick: function (d, i) {
          console.log("onclick", d, i);
        },
        onmouseover: function (d, i) {
          console.log("onmouseover", d, i);
        },
        onmouseout: function (d, i) {
          console.log("onmouseout", d, i);
        }
      },
      donut: {
        title: "example"
      }
    });

    var chart4 = c3.generate({
      bindto: '#chart4',
      size: {
        width: 150,
        height: 180
      },
      padding: {
        top: 20,
        right: 20
      },
      color: {
        pattern: ['#D62728', '#DEA1A1']
      },
      data: {
        columns: [
          ['data1', 30],
          ['data2', 120]
        ],
        type: 'donut',
        onclick: function (d, i) {
          console.log("onclick", d, i);
        },
        onmouseover: function (d, i) {
          console.log("onmouseover", d, i);
        },
        onmouseout: function (d, i) {
          console.log("onmouseout", d, i);
        }
      },
      donut: {
        title: "example"
      }
    });

  }
})();
